import MainLayout from '../../components/MainLayout';

export default function QRCode() {
    return (
        <MainLayout>
            <div className="page-header text-center mx-auto">
                <h1 className="display-4">QR Code Labels</h1>
            </div>
        </MainLayout>
    )
};
