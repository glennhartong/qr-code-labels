import MainLayout from '../components/MainLayout';
import Link from 'next/link';

import './stylesheets/projects.css';

export default function Projects() {
    return (
        <MainLayout>
            <div className="page-header text-center mx-auto">
                <h1 className="display-4">Projects</h1>
            </div>
            <div className="card-row container">
                <div className="project-card card text-white card-body">
                    <div className="card-title">QR Code Labels</div>
                    <p className="card-text">Create labels with QR codes for printing by entering part details.</p>
                    <a href="/project/qr-code" className="btn btn-secondary">Open</a>
                </div>
                <div className="project-card card text-white card-body">
                    <div className="card-title">Project 2</div>
                    <p className="card-text">labels details with Create entering by for codes part printing QR</p>
                    <Link href="/about">
                        <a className="btn btn-secondary">Open</a>
                    </Link>
                </div>
                <div className="project-card card text-white card-body">
                    <div className="card-title">Something else</div>
                    <p className="card-text">Create labels with QR codes for printing by entering part details.</p>
                    <Link href="/about">
                        <a className="btn btn-secondary">Open</a>
                    </Link>
                </div>
                <div className="project-card card text-white card-body">
                    <div className="card-title">Project Cinco</div>
                    <p className="card-text">Create labels with QR codes for printing by entering part details.</p>
                    <Link href="/about">
                        <a className="btn btn-secondary">Open</a>
                    </Link>
                </div>
            </div>
        </MainLayout>
    )
};
