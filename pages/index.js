import MainLayout from '../components/MainLayout';
import {
  Container,
  Row,
  Col,
  Jumbotron,
  Button
} from 'reactstrap';

export default function Index() {
  return (
    <MainLayout>
      <Jumbotron>
        <Container>
          <Row>
            <Col>
              <h1>Welcome to Rick Berg {'{.net}'}</h1>
              <h2>The place for all things Rick.</h2>
              <p>Other blah blah blah text here.</p>
              <Button color={'danger'} onClick={() => alert('ouch')}> Hit me </Button>
            </Col>
          </Row>
        </Container>
      </Jumbotron>
    </MainLayout>
  );
}
