# QR Code Labels

quick app to print qr codes to a label from part detail input.

`npm run dev` to start dev server
`npm run build` to buld nextjs files
`npm run export` to export production files
`cd out` && `serve -p 8080` to run "Serve" server and view
`now --prod` to deploy to production (from `out`)
`now` to deploy to stage (from `out`)

View at [http://now.rickberg.net]