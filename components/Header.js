import React, { useState } from 'react';
import Link from 'next/link';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './stylesheets/header.css';

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <Navbar color="inverse" light expand="md">
        <NavbarBrand href="/">Rick Berg {'{.net}'}</NavbarBrand>
        <NavbarToggler onClick={() => setIsOpen(!isOpen)} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <Link href="/" passHref>
                <NavLink>Home</NavLink>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="/about" passHref>
                <NavLink>About</NavLink>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="/projects" passHref>
                <NavLink>Projects</NavLink>
              </Link>
            </NavItem>
            <NavItem>
              <NavLink href="https://bitbucket.org/glennhartong/qr-code-labels/src/master/" target={'_blank'}>BitBucket</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  )
};

export default Header;
