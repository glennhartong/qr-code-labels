import Header from './Header';

const layoutStyle = {
  padding: 20,
  border: '1px solid #DDD',
  width: '100 vw'
};

const MainLayout = props => (
  <div style={layoutStyle}>
    <Header />
    {props.children}
  </div>
);

export default MainLayout;
