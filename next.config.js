const withCSS = require('@zeit/next-css')

module.exports = withCSS({
    exportPathMap: async function() {
      const paths = {
        '/': { page: '/' },
        '/about': { page: '/about' },
        '/projects': { page: '/projects' },
        '/project/qr-code': { page: '/project/qr-code' }
      };
    
      return paths;
    }
  });
